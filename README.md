# online-order-vue
## Products reservation multistep form.
![Form Image](image.png)
# Overview
- asynchronous sending and receiving of data from the server
- server-side asynchronous form validation
- dynamic steps progress bar
- delivery address autocomplete
- dynamic multiselect of services 
- dynamic calendar
- Stripe ( online payments ) integration

# Installation & Development
## Project setup
```
yarn install
```

## Compiles and hot-reloads for development
```
yarn run serve
```

## Compiles and minifies for production
```
yarn run build
```

## Run your tests
```
yarn run test
```

## Lints and fixes files
```
yarn run lint
```

# Components
```
ProgressForm
popupEror
vueCallendar
FormStepFirst
FormStepTwo
FromStepThree
FromStepFour
FromStepFive
FromStepFinish
    -- Integrating Stripe Elements and Vue
thankPage
```

# Node Modules
**Axios** - [Configuration Reference](https://github.com/axios/axios)
<br> 
**Google-autocomplete** - [Configuration Reference](https://www.npmjs.com/package/vue-google-autocomplete)
<br> 
**Multiselect** - [Configuration Reference](https://vue-multiselect.js.org/)

# Vue config
See [Configuration Reference](https://cli.vuejs.org/config/#assetsdir)

# Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
